const webpack = require('webpack');
const CompressionPlugin = require('compression-webpack-plugin');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const CopyWebpackPlugin = require('copy-webpack-plugin');

const configureWebpack = {
  plugins: [
    // 引入JQuery
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery'
    }),
  ],
  optimization: {
    splitChunks: {
      chunks: 'all',
      maxInitialRequests: 100,
      cacheGroups: {
        libs: { // 第三方库
          name: "chunk-libs",
          test: /[\\/]node_modules[\\/]/,
          priority: 10,
          chunks: "initial"
        },
        vue: { // vue 单独拆包
          name: "chunk-vue",
          test: /[\\/]node_modules[\\/]vue[\\/]/,
          priority: 20,
          chunks: "initial"
        },
        vuex: { // vuex 单独拆包
          name: "chunk-vuex",
          test: /[\\/]node_modules[\\/]vuex[\\/]/,
          priority: 20,
          chunks: "initial"
        },
        'vue-router': { // vue-router 单独拆包
          name: 'chunk-vue-router',
          test: /[\\/]node_modules[\\/]vue-router[\\/]/,
          priority: 20,
          chunks: "initial"
        },
        iview: { // iview 单独拆包
          name: "chunk-iview",
          test: /[\\/]node_modules[\\/]iview[\\/]/,
          priority: 20,
          chunks: "initial"
        },
        'view-design': { // view-design 单独拆包
          name: "chunk-view-design",
          test: /[\\/]node_modules[\\/]view-design[\\/]/,
          priority: 20,
          chunks: "initial"
        },
        echarts: { // echarts 单独拆包
          name: "chunk-echarts",
          test: /[\\/]node_modules[\\/]echarts[\\/]/,
          priority: 20,
          chunks: "initial"
        },
        'echarts-gl': { // echarts-gl 单独拆包
          name: "chunk-echarts-gl",
          test: /[\\/]node_modules[\\/]echarts-gl[\\/]/,
          priority: 20,
          chunks: "initial"
        },
        jshint: { // jshint 单独拆包
          name: "chunk-jshint",
          test: /[\\/]node_modules[\\/]jshint[\\/]/,
          priority: 20,
          chunks: "initial"
        },
        codemirror: { // codemirror 单独拆包
          name: "chunk-codemirror",
          test: /[\\/]node_modules[\\/]codemirror[\\/]/,
          priority: 20,
          chunks: "initial"
        },
        'vue-meditor': { // vue-meditor 单独拆包
          name: "chunk-vue-meditor",
          test: /[\\/]node_modules[\\/]vue-meditor[\\/]/,
          priority: 20,
          chunks: "initial"
        },
        'jquery': { // jquery 单独拆包
          name: "chunk-jquery",
          test: /[\\/]node_modules[\\/]jquery[\\/]/,
          priority: 20,
          chunks: "initial"
        },
        commons: { // 公共模块包
          name: 'chunk-commons',
          minChunks: 2,
          priority: 0,
          reuseExistingChunk: true
        }
      }
    }
  }
  // externals:{
  //   'BMap': 'BMap',
  //   'BMap_Symbol_SHAPE_POINT':'BMap_Symbol_SHAPE_POINT'
  // }
};

if (process.env.NODE_ENV === 'production') {
  configureWebpack.plugins.push(
    // 使用Gzip压缩
    new CompressionPlugin({
      test: /\.js$|\.html$|\.css/,
      threshold: 10240,
      deleteOriginalAssets: false
    }),
    // 打包分析插件
    new BundleAnalyzerPlugin(),
    // 实现自动在打包后的dist目录中添加WEB-INF/web.xml文件
    new CopyWebpackPlugin([
      {
        from: __dirname + '/public/WEB-INF/web.xml',
        to: __dirname + '/dist/WEB-INF'
      },
      {
        from: __dirname + '/public/static/config_prod.js',
        to: __dirname + '/dist/static/config.js',
        force: true
      }
    ])
  )
}

module.exports = {
  publicPath: '/pagenow', // 此变量不建议修改
  assetsDir: './static', // 放置生成的静态资源 (js、css、img、fonts) 的 (相对于 outputDir 的) 目录。
  // 配置less
  css: {
    loaderOptions: {
      less: {
        javascriptEnabled: true,
      }
    },
  },
  devServer: {
    port: 8081, // 生产环境启动端口
    // proxy: {
    //   '/pagenow': {
    //     target: 'http://localhost:8090',
    //     ws: true,
    //     changOrigin: true,
    //     pathRewrite: {
    //       "^/pagenow": ""
    //     }
    //   }
    // }
  },
  configureWebpack: configureWebpack,
  chainWebpack: config => {
    // 移除 prefetch 插件
    config.plugins.delete('preload');
    config.plugins.delete('prefetch');
    config.module
      .rule('swf')
      .test(/\.swf$/)
      .use('url-loader')
      .loader('url-loader')
      .options({
        limit: 10000
      })
    config.module
      .rule('text-loader')
      .test(/.md$/)
      .use('text-loader')
        .loader('text-loader')
        .end()
  },
  // 关闭SourceMap的生成，防止打包后的文件被反编译
  productionSourceMap: false,
  runtimeCompiler: true
};

