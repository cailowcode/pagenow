## 外部如何触发抽屉的显示与隐藏

抽屉式IFrame组件内部定义了data属性visible，用于绑定抽屉组件的显示与隐藏状态，因此外部组件想要控制抽屉的显示与隐藏，只需要
动态修改visible属性即可，例如外部通过按钮组件控制抽屉式IFrame的显示可以在按钮组件的点击时触发脚本中获取抽屉式IFrame的VM实例，
并修改visible属性即可，示例代码如下：

```javascript
this.findCompVmById('抽屉式IFrame组件ID').visible = true
```
