## 脚本中常用配置属性说明

- this.component.compConfigData.name：对应iframe的name属性
- this.component.compConfigData.srcPath：链接地址
- this.component.compConfigData.width：iframe的宽度【Number类型】
- this.component.compConfigData.height：iframe的高度【Number类型】

## 基础配置

- 【链接地址】会覆盖数据源中的结果集数据，如果需要使用数据源动态获取IFrame的src属性，那么就不要配置链接地址

## 交互

### 如何向IFrame内传输数据
```javascript
this.iframeWin.postMessage({
 type: 'sendMessage', 
 params: {}
}, '*')
```
我们可以在【初始化运行脚本】中向IFrame内部页面发送数据，例如：监听一条事件总线事件【btn-click】，当监听到之后，向IFrame内发送消息
```javascript
this.$EventBus.receive(['btn-click'], (params) => {
  this.iframeWin.postMessage({ 
    type: 'sendMessage',
    params: params}, '*')
})
```
### IFrame内部页面如何接收数据
```javascript
window.addEventListener('message', function (event) {
  let data = event.data;
  switch (data.type) {
    case 'sendMessage':
      console.log(data.params);
    break;
  }
});
```
### 消息监听队列
- PageNow中的IFrame组件可以通过添加多个消息监听队列，用于接收内部页面发送出来的消息。
- 一条监听队列可以配置【监听类型】与【处理脚本】，内部页面通过如下代码发送消息：
```javascript
window.parent.postMessage({
      type: 'sendMessage',
      params: '我是内部页面发送给外部的消息'
    }, '*');
```
内部页面发送的消息的信息载体必须含有两个属性，type与params，一条监听队列中的监听类型与type对应即可接收此条消息并运行处理脚本，下面演示
如何在运行脚本中接收内部页面发送回来的params
```javascript
console.log(data.params)
```
- 注意：监听队列的运行脚本中，全局变量【this】已切换为【_this】，例如需要在运行脚本中发送一条事件总线，那么我们需要这么写
```javascript
_this.$EventBus.send('eventName', 'hello world!')
```
- 编辑【消息监听队列】之后，需要点击保存更改，才可以使编辑后的信息生效，值得注意的是，这里的保存更改只是保存当前IFrame的消息监听队列信息，
并不会保存页面
