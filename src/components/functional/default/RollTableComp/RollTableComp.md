

## 交互
当单元格被点击时（表头不支持），轮播表将抛出一个click事件，包含被点击的单元格的相关数据，通过params可以获取相关参数信息。

| 属性  | 说明 |
| ------------- | ------------- |
| params.row  | 所在行数据  |
| params.ceil  | 单元格数据  |
| params.rowIndex  | 行索引  |
| params.columnIndex  | 列索引  |

当然，你也可以在点击事件处理脚本中，send一个全局事件总线，将params相关信息发送出去，实现不同组件间的交互。示例：

```javascript
EventBus.send('rollTable-row-click', params.row)
```
