import PnUtil from "@/utils/PnUtil";

function buildTableColumnConfig (otherConfig) {
  return PnUtil.deepMerge({
    key: '',
    title: '',
    align: 'left',
    className: '',
    width: 0,
    fixed: '',
    ellipsis: false,
    tooltip: false
  }, otherConfig)
}

export default {
  buildTableColumnConfig
}
