const buildSeriesObj = function (name = '', color = '') {
  return {
    name: name,
    selected: false,
    itemStyle: {
      color: color,
      borderColor: '#000',
      borderWidth: 0
    }
  }
};

export default {
  buildSeriesObj
}
