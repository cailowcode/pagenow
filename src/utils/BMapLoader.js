const ak = '9dIsqTpAdOlGjHTCLeaNilC2X0igqO9j'

export default function load() {
  return new Promise(function(resolve, reject) {
    if (typeof BMapGL !== 'undefined') {
      resolve(BMapGL)
      return true
    }
    window.onBMapCallback = function() {
      resolve(BMapGL)
    }

    let mapScript = document.createElement('script')
    mapScript.type = 'text/javascript'
    mapScript.src =
      'https://api.map.baidu.com/api?v=3.0&type=webgl&ak=' + ak + '&callback=onBMapCallback'
    mapScript.onerror = reject
    document.head.appendChild(mapScript)

    let mapvglScript = document.createElement('script')
    mapvglScript.type = 'text/javascript'
    mapvglScript.src =
      'https://code.bdstatic.com/npm/mapvgl@1.0.0-beta.124/dist/mapvgl.min.js'
    document.head.appendChild(mapvglScript)
  })
}
