import PnUtil from "@/utils/PnUtil";

export const COL_NUM = 12;
export const LAYOUT_ITEM_W = 3;
export const LAYOUT_ITEM_H = 6;

const buildInitPageMetadata = function () {
  let pageMetadata = {
    layout: {
      layoutItems: [

      ]
    }
  };
  return pageMetadata
};

/**
 * 构建 响应式栅格布局默认配置
 * @returns
 */
const buildReactiveLayoutConfigData = function () {
  let layoutConfigData = {
    canvasWidth: 0,

    colNum: COL_NUM, // 定义栅格系统的列数，其值需为自然数
    rowHeight: 30, // 每行的高度，单位像素
    maxRows: 200, // 定义最大行数
    marginHorizontal: 10,
    marginVertical: 10,
    isDraggable: true, // 标识栅格中的元素是否可拖拽
    isResizable: true, // 标识栅格中的元素是否可调整大小
    isMirrored: false, // 标识栅格中的元素是否可镜像反转
    autoSize: true, // 标识容器是否自动调整大小
    verticalCompact: true, // 标识布局是否垂直压缩
    preventCollision: false, // 防止碰撞属性，值设置为ture时，栅格只能拖动至空白处
    useCssTransforms: true, // 标识是否使用CSS属性 transition-property: transform;
    responsive: false, // 标识布局是否为响应式

    backgroundColor: '#0E2B43',
    syncSetPageBgColor: true, // 是否同步设置页面背景色

    imageRelativePath: '',
    imageRepeat: 'no-repeat',
    imageSize: '100% 100%',

    styleFilter_use: false,
    styleFilter_hueRotate: 0,
    styleFilter_contrast: 100,
    styleFilter_opacity: 100,
    styleFilter_saturate: 100,
    styleFilter_brightness: 100,

    customStyleCode: '{\n}',

    pageGlobalStyleCode: '',  // 页面全局样式
    designPhaseNotLoadGlobalStyle: false, // 设计阶段不加载页面全局样式
    pageGlobalJsCode: 'if(!PnUtil.isDesignerPage()){\n  //此处编辑脚本主体\n  \n}',     // 页面全局JS脚本

    hideHttpLoadingBar: true, // 页面发布后是否显示数据请求时的进度条
  };
  return layoutConfigData
};

/**
 * 构建 响应式栅格布局布局块对象
 * @param x
 * @param y
 * @returns
 */
const buildReactiveLayoutItem = function (x = 0, y = 0) {
  let layoutItem = {
    id: PnUtil.uuid(),
    name: 'DefaultLayoutItem',
    aliasName: '',
    layoutItemConfigData: {
      className: '',

      w: LAYOUT_ITEM_W, // 标识栅格元素的初始宽度，值为colNum的倍数
      h: LAYOUT_ITEM_H, // 标识栅格元素的初始高度，值为rowHeight的倍数
      x: x, // 标识栅格元素位于第几列，需为自然数
      y: y, // 标识栅格元素位于第几行，需为自然数

      draggableEnabled: true, // 标识栅格中的元素是否可拖拽
      resizableEnabled: true, // 标识栅格中的元素是否可调整大小
      isStatic: false, // 标识栅格元素是否为静态的（无法拖拽、调整大小或被其他元素移动）
      dragIgnoreFrom: 'a, button',

      backgroundColor: '',
      padding: 0,

      borderWidth: 0,
      borderStyle: 'solid',
      borderColor: '#000',

      borderTopLeftRadius: 0,
      borderTopRightRadius: 0,
      borderBottomLeftRadius: 0,
      borderBottomRightRadius: 0,

      boxShadowInset: false,
      boxShadowLeft: 0,
      boxShadowTop: 0,
      boxShadowRadius: 0,
      boxShadowColor: '',

      zIndex: 2,

      animationVisible: false,
      animationDelay: '',
      inAnimation: '',
      outAnimation: '',

      rotationAngle: 0, // 旋转角度
      flipHorizontal: false, // 水平翻转
      flipVertical: false, // 垂直翻转

      use3dRotate: false, // 是否启用3D旋转
      axisOfRotation: 'y', // 旋转轴
      rotationAngleFor3d: 30, // 旋转角度
      perspectiveFor3d: 500, // 透视距离

      opacity: 1,
      display: 'block',

      customStyleCode: '{\n}',

      imageRelativePath: '',
      imageRepeat: 'no-repeat',
      imageSize: '100% 100%',

    },
    component: {
      id: '',
      name: '',
      compConfigData: {

      }
    }
  };

  return layoutItem
};

export default {
  buildInitPageMetadata,
  buildReactiveLayoutConfigData,
  buildReactiveLayoutItem
}
