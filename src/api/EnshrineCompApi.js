import {Axios} from '../utils/AxiosPlugin'

const saveEnshrineCompGroup = async function (enshrineCompGroup) {
  return await Axios.post('/enshrineComp/saveEnshrineCompGroup', enshrineCompGroup);
};

const getAllEnshrineCompGroups = async function () {
  return await Axios.get('/enshrineComp/getAllEnshrineCompGroups');
};

const getEnshrineCompGroupByPage = async function (pageIndex, pageSize) {
  return await Axios.post('/enshrineComp/getEnshrineCompGroupByPage', {pageIndex: pageIndex, pageSize: pageSize});
};

const deleteEnshrineCompGroup = async function (ids) {
  return await Axios.delete('/enshrineComp/deleteEnshrineCompGroup', {params: {ids: ids}});
};

const getEnshrineCompGroupById = async function (id) {
  return await Axios.get('/enshrineComp/getEnshrineCompGroupById', {params: {id: id}})
}

const saveEnshrineComp = async function (enshrineComp) {
  return await Axios.post('/enshrineComp/saveEnshrineComp', enshrineComp);
};

const getEnshrineCompByPage = async function (pageIndex, pageSize, group_id) {
  return await Axios.post('/enshrineComp/getEnshrineCompByPage', {pageIndex: pageIndex, pageSize: pageSize, group_id: group_id});
};

const getLayoutItemObjById = async function (id) {
  return await Axios.get('/enshrineComp/getLayoutItemObjById', {params: {id: id}});
};

const deleteEnshrineComp = async function (ids) {
  return await Axios.delete('/enshrineComp/deleteEnshrineComp', {params: {ids: ids}});
};

const buildEnshrineCompLibrary = async function () {
  return await Axios.get('/enshrineComp/buildEnshrineCompLibrary');
};

const searchEnshrineComp = async function (enshrine_name) {
  return await Axios.post('/enshrineComp/searchEnshrineComp', {enshrine_name: enshrine_name})
};

export default {
  saveEnshrineCompGroup,
  getAllEnshrineCompGroups,
  getEnshrineCompGroupByPage,
  deleteEnshrineCompGroup,
  getEnshrineCompGroupById,
  saveEnshrineComp,
  getEnshrineCompByPage,
  getLayoutItemObjById,
  deleteEnshrineComp,
  buildEnshrineCompLibrary,
  searchEnshrineComp
}
