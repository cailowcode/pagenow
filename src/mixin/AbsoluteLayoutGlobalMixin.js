
const AbsoluteLayoutGlobalMixin = {
  data() {
    return {

    }
  },
  provide() {
    return {
      setChildrenRef: (componentId, ref) => {
        this[componentId] = ref
      },
      getChildrenRef: (componentId) => {
        if (this[componentId]) {
          return this[componentId]
        }else {
          this.$Message.error({
            content: '函数findCompVmById找不到指定ID【'+componentId+'】的组件实例',
            duration: 4
          })
        }
      },
      getThisRef: () => {
        return this
      }
    }
  },
  created () {

  },
  methods: {
    buildEnterActiveClass (layoutItem) {
      if(this.$store.state.release.pageMetadata) {
        if(layoutItem.layoutItemConfigData.animationVisible) {
          return 'animated ' + layoutItem.layoutItemConfigData.inAnimation + ' ' + layoutItem.layoutItemConfigData.animationDelay;
        }else {
          return ''
        }
      }
    },
    buildLeaveActiveClass (layoutItem) {
      if(this.$store.state.release.pageMetadata) {
        if(layoutItem.layoutItemConfigData.animationVisible) {
          return 'animated ' + layoutItem.layoutItemConfigData.outAnimation + ' ' + layoutItem.layoutItemConfigData.animationDelay;
        }else {
          return ''
        }
      }
    },

    /**
     * 构建用户自定义的class名称
     * @param layoutItem
     * @returns {string}
     */
    buildLayoutItemCustomClassName (layoutItem) {
      let clazz = '';
      if (layoutItem.layoutItemConfigData.className) {
        clazz = ' ' + layoutItem.layoutItemConfigData.className
      }
      return clazz
    },

    /**
     * 构建布局块被隐藏后添加class[hidden]类名
     * @param layoutItem
     * @returns {string}
     */
    buildLayoutItemHiddenClassName (layoutItem) {
      if (layoutItem.layoutItemConfigData.display == 'none') {
        return ' hidden'
      }
      return ''
    },

    /**
     * 构建3d旋转样式
     * @param layoutItem
     * @returns {string}
     */
    buildLayoutItem3dTransformStyleValue (layoutItem) {
      let style = 'translate3d(0px, 0px, 0px) scale3d(1, 1, 1)';
      if (layoutItem.layoutItemConfigData.use3dRotate) {
        if (layoutItem.layoutItemConfigData.axisOfRotation == 'x') {
          style += ' rotate3d(1, 0, 0, ' + layoutItem.layoutItemConfigData.rotationAngleFor3d + 'deg)'
        }else if (layoutItem.layoutItemConfigData.axisOfRotation == 'y') {
          style += ' rotate3d(0, 1, 0, ' + layoutItem.layoutItemConfigData.rotationAngleFor3d + 'deg)'
        }else if (layoutItem.layoutItemConfigData.axisOfRotation == 'xy') {
          style += ' rotate3d(1, 1, 0, ' + layoutItem.layoutItemConfigData.rotationAngleFor3d + 'deg)'
        }
      }else {
        style = 'none'
      }
      return style
    },

    /**
     * 构建滤镜样式
     * @param layoutConfigData
     * @returns {string}
     */
    buildLayoutItemStyleFilter (layoutConfigData) {
      let clazz = '';
      if (layoutConfigData.styleFilter_use) {
        clazz =
          ' hue-rotate(' + layoutConfigData.styleFilter_hueRotate + 'deg)' +
          ' contrast(' + layoutConfigData.styleFilter_contrast + '%)' +
          ' opacity(' + layoutConfigData.styleFilter_opacity + '%)' +
          ' saturate(' + layoutConfigData.styleFilter_saturate + '%)' +
          ' brightness(' + layoutConfigData.styleFilter_brightness + '%)';
      }
      return clazz
    }
  },
  computed: {

  },
  asyncComputed: {
    canvasBackgroundImageSrc () {
      return window.g.AXIOS_BASE_URL + this.layout.layoutConfigData.imageRelativePath
      /*return new Promise(resolve => {
        this.$PnUtil.imageToBase64(window.g.AXIOS_BASE_URL + '/' + this.layout.layoutConfigData.imageRelativePath, 'png', function (base64) {
          resolve(base64)
        })
      })*/
    }
  }
};

export default AbsoluteLayoutGlobalMixin
